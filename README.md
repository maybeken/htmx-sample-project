# HTMX Sample Project

## Introduction
I am just testing out HTMX and the capability of it to host static web site. Playing around to understand what works and what doesn't.

## Usage
1. Running `hacks/install-dep.sh` to install required dependencies from unpkg.com
2. Upload everything to a web server / Amazon S3 / local http-server
3. Profit?

## Custom Features
* Dependencies installation without NPM / YARN / PNPM etc.
* Implementation of route management (Similar to Vue.js using anchor hash)
* Modified loading indicator CSS styling