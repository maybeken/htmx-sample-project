#!/bin/bash

## Download HTMX & dependencies
HTMX_VERSION="1.9.2"
curl -L -o ./js/htmx.min.js "https://unpkg.com/htmx.org@$HTMX_VERSION/dist/htmx.min.js"
curl -L -o ./js/htmx-json-enc.min.js "https://unpkg.com/htmx.org@$HTMX_VERSION/dist/ext/json-enc.js"
curl -L -o ./js/htmx-templates.js "https://unpkg.com/htmx.org@$HTMX_VERSION/dist/ext/client-side-templates.js"

## Download Idiomorph DOM morphing library
IDIOMORPH_VERSION="0.0.8"
curl -L -o ./js/idiomorph-ext.min.js "https://unpkg.com/idiomorph@$IDIOMORPH_VERSION/dist/idiomorph-ext.min.js"

## Download Mustache template engine
MUSTACHE_VERISON="4.2.0"
curl -L -o ./js/mustache.min.js "https://unpkg.com/mustache@$MUSTACHE_VERISON/mustache.min.js"